public class Main {
    public static void main(String[] args) {

        String data = "pop,land\n1412,CN\n1387,IN\n333,USA\n271,INA";

        String column = "pop";
        Main main = new Main();
        System.out.println(main.solution(data, column));
    }

    public int solution(String S, String C) {

        String[] numRows = S.split("\n"); //numbers of rows
        String[] numColumns = numRows[0].split(","); //numbers of columns

        String[][] matrix = new String[numRows.length][numColumns.length];

        //creating 2D array
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                matrix[row][col] = S.split("\n")[row].split(",")[col];
            }
        }

        String[] targetColumn = new String[numRows.length];
        int max = Integer.MIN_VALUE;
        int count = 0;

        //recognize the position of column with numbers
        for (int i = 0; i < matrix[0].length; i++) {
            if (C.equals(matrix[0][i])) {
                count = i;
            }
        }

        // copy the elements of target column into separate array
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                targetColumn[i] = matrix[i][count];
            }
        }

        //finding maximum int value excepting 1st index (name of column)
        for (int i = 1; i < targetColumn.length; i++) {
            if (Integer.parseInt(targetColumn[i]) > max) {
                max = Integer.parseInt(targetColumn[i]);
            }
        }

        return max;
    }
}